var ns = require('node-schedule');
var _ = require('underscore');
/*
*    *    *    *    *    *
┬    ┬    ┬    ┬    ┬    ┬
│    │    │    │    │    |
│    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
│    │    │    │    └───── month (1 - 12)
│    │    │    └────────── day of month (1 - 31)
│    │    └─────────────── hour (0 - 23)
│    └──────────────────── minute (0 - 59)
└───────────────────────── second (0 - 59, OPTIONAL)
*/
function Scheduler( logger ) {
    if (! _.isUndefined( logger ) ){
        this.logger = logger;
    }
    this.settings = {};
}

Scheduler.prototype.schedule = function( params ) {
    
    if( _.isObject( params ) ) {
        if ( _.isUndefined( params['name'] ) ) {
            throw new Error("Schedule name required..");
        }
        if ( _.isUndefined( params['lib'] ) ) {
            throw new Error("Schedule lib required..");
        }
        if ( _.isUndefined( params['call'] ) ) {
            throw new Error("Schedule call required..");
        }
    } else {
        throw new Error("No config supplied");
    }

    var rule = new ns.RecurrenceRule();
    [ 'second', 'minute', 'hour', 'dayOfWeek', 'dayOfMonth'].forEach( function( interval) {
        if (!_.isUndefined(params[ interval ])){
            rule[ interval ] = params[ interval ];
        }
    });

    var Lib = require( params['lib'] );
    var lib = new Lib;

    var logger = this.logger;
    this.settings[ params['name'] ] = ns.scheduleJob( rule, function( ){
        logger.info('Scheduler calling '+params['name']+' start');

        lib[ params['call'] ]( params['input'], function(error, result ) {
            if (error) {
                logger.error('Scheduled call '+params['name']+' error: '+error);
            } else {
                logger.info('Scheduled call '+params['name']+' complete: '+result);
            }
        }); 
    });

    logger.info(params['name']+ ' scheduled');
};
module.exports = Scheduler;
