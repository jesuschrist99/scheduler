'use strict';

var assert = require('assert');
var Scheduler = require('../lib/scheduler.js');

describe('Scheduler', function() {
    describe('constructor', function() {
        it('should create empty settings', function() {
            assert.deepEqual(new Scheduler().settings, {});
        });
        it('should use provided logger', function() {
            var logger = {};
            assert(new Scheduler(logger).logger === logger)
        });
        // uncomment this test and make the constructor create its own logger(?)
        // it("maybe should be able to create its own logger?", function() {
        //   assert.doesNotThrow(function() {
        //     new Scheduler().schedule({name: "a", lib: '../lib/scheduler.js', call: "what is call?"});
        //   }, "did not supply a logger", "this should not explode");
        // });
    });
    describe('#schedule()', function() {
        it('should throw error with no arg', function() {
            assert.throws(function() {
                new Scheduler().schedule()
            }, "expected dummy spit", "schedule requires an option object");
        });
        it('should require name, lib and call', function() {
            var logger = {};
            var s = new Scheduler(logger);
            assert.throws(function() {
                s.schedule({})
            });
            assert.throws(function() {
                s.schedule({name: "I am foobar", lib: "thelib"});
            });
            assert.throws(function() {
                s.schedule({name: "I am foobar", call: "thecall"});
            });
            assert.throws(function() {
                s.schedule({lib: "thelib", call: "thecall"});
            });
        });

        it('should register job under name property in settings', function() {
            var logLines = [];
            var logger = {
                info: function(it) {
                    logLines.push(it);
                }
            };
            var s = new Scheduler(logger);
            s.schedule({name: "a", lib: '../lib/scheduler.js', call: "what is call?"});
            assert(!!s.settings.a, "expected a job listed under the name");
            assert(!!s.settings.a.job);
            assert(logLines.length > 0);
        });
    });
});
